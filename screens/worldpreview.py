from gamescreen import GameScreen
import savefile
import game
import colors
import pygame
import gui
from translate import _


class WorldPreview(GameScreen):
    
    def __init__(self):
        self.world = None
        self.players = []
        self.playerButtons = []
        self.selected = -1
        super(WorldPreview, self).__init__()
        self.noone = gui.TextBox((10,10), _('No one character'))

    def load(self, config):
        super(WorldPreview, self).load(config)
        assert('world' in config)
        assert(isinstance(config['world'], savefile.World))
        self.world = config['world']
        self.reset()
        
    def reset(self):
        super().reset()
        self.selected = -1
        self.buttons = []
        self.playerButtons = []
        buttonWidth = 150
        buttonHeight = 40
        if self.world:
            self.players = self.world.getPlayers()
            i = 0
            for player in self.players:
                btn = gui.Button((self.screenWidth-buttonWidth-10, 10+i*40, buttonWidth, buttonHeight-10), _('Select'),
                                 click=lambda screen, btn: 0)
                self.buttons.append(btn)
                self.playerButtons.append(btn)

                box = gui.TextBox((10, 15+i*40), player.name)
                self.textboxes.append(box)
                i += 1

        backBtn = gui.Button((self.screenWidth - buttonWidth - 10, self.screenHeight - buttonHeight - 10,
                              buttonWidth, buttonHeight), _('[Esc] Back'),
                             lambda screen, btn: game.change_screen('SelectWorld'), key=pygame.K_ESCAPE)
        self.buttons.append(backBtn)

        createBtn = gui.Button((self.screenWidth - buttonWidth*3 - 20, self.screenHeight - buttonHeight - 10,
                                buttonWidth*2, buttonHeight), _('[n] New character'),
                               lambda screen, btn: game.change_screen('CreateCharacter', self.config, True),
                               key=pygame.K_n)
        self.buttons.append(createBtn)

    def up(self):
        if len(self.playerButtons) == 0:
            return
        if self.selected == -1 or self.selected == 0:
            self.selected = len(self.worldButtons) - 1
        else:
            self.selected -= 1
        self.activate()

    def down(self):
        if len(self.playerButtons) == 0:
            return
        if self.selected == -1 or self.selected == len(self.playerButtons) - 1:
            self.selected = 0
        else:
            self.selected += 1
        self.activate()

    def activate(self):
        i = 0
        for btn in self.playerButtons:
            if i == self.selected:
                btn.activeOn()
            else:
                btn.activeOff()
            i += 1
        self.redraw()

    def draw(self):
        game.staticSurface.fill(colors.Black)
        if len(self.players) == 0:
            self.noone.draw()
        else:
            for i in range(len(self.players)):
                pygame.draw.rect(game.staticSurface, colors.LightSteelBlue, (0, 5+i*40, self.screenWidth, 40), 1)
                if i == self.selected:
                    pygame.draw.rect(game.staticSurface, colors.DarkSlateGray, (1, 6+i*40, self.screenWidth-2, 38))
        super().draw()

    def call(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_DOWN:
                self.down()
            elif event.key == pygame.K_UP:
                self.up()
        super().call(event)
        # if event.type == 'KEYDOWN':
        #     if event.key == 'ESCAPE':
        #         Game.changeScreen('SelectWorld')
        #     elif event.key == 'DOWN':
        #         self.selected += 1
        #         if self.selected > self.inputsCount - 1:
        #             self.selected = 0
        #     elif event.key == 'UP':
        #         self.selected -= 1
        #         if self.selected < 0:
        #             self.selected = self.inputsCount - 1
        #     elif event.key == 'ENTER':
        #         pass
        #     elif event.key == 'CHAR':
        #         if event.char == 'c':
        #             Game.changeScreen('CreateUnit', {'world': self.world}, True)
        #         elif event.char == 'v':
        #             Game.changeScreen('WorldPreviewMap', {'world': self.world}, True)

    def tick(self):
        pass
