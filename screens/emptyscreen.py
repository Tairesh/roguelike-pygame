from gamescreen import GameScreen
import game
import colors


class EmptyScreen(GameScreen):
    def __init__(self):
        super().__init__()
        self.reset()

    def reset(self):
        super().reset()

    def clear(self):
        super().clear()
        game.surface.fill(colors.Black)

    def call(self, event):
        super().call(event)

    def draw(self):
        super().draw()
