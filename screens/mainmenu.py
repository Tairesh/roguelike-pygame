import pygame
import colors
import gui
import game
from translate import _
from gamescreen import GameScreen
import milkyway


class MainMenu(GameScreen):
    '''
    Main menu
    '''

    def __init__(self):
        super(MainMenu, self).__init__()
        menuItems = (
            (_('[l] Load world'), lambda screen, btn: game.change_screen('SelectWorld'), pygame.K_l),
            (_('[c] Create new world'), lambda screen, btn: game.change_screen('CreateWorld', [], True), pygame.K_c),
            (_('[e] Test'), lambda screen, btn: game.change_screen('TestScreen', [], True), pygame.K_e),
            (_('[t] Settings'), lambda screen, btn: game.change_screen('SettingsMenu'), pygame.K_t),
            (_('[x] Exit'), lambda screen, btn: exit(0), pygame.K_x),
        )
        buttonWidtn = 250
        buttonHeight = 40
        buttonOffset = 10
        x = (self.screenWidth - buttonWidtn) / 2
        y = (self.screenHeight - (buttonHeight + buttonOffset) * len(menuItems) - 20)
        self.menuTop = y
        for name, click, key in menuItems:
            btn = gui.Button(pygame.Rect(x, y, buttonWidtn, buttonHeight), name, click, key=key)
            self.buttons.append(btn)
            y += buttonHeight + buttonOffset

        # print(milkyway.generate_sector(5912, 5412, 0))

    def draw(self):
        # game.surface.blit(images.LOGO, (self.screenWidth/2 - images.LOGO.get_width()/2, 50))
        SCALE = 3
        ox = self.screenWidth/2-SCALE*128/2
        oy = self.menuTop/2-SCALE*128/2
        for x in range(128):
            for y in range(128):
                c = milkyway.MILKY_WAY[x*128+y]
                pygame.draw.rect(game.surface, (c*135/256, c*206/256, c*250/256), (ox+x*SCALE,oy+y*SCALE,SCALE,SCALE))

        super().draw()

    def clear(self):
        game.surface.fill(colors.Black)
