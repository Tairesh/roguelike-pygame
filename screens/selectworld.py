from gamescreen import GameScreen
import savefile
import game
import colors
import pygame
import gui
from translate import _


class SelectWorld(GameScreen):
    
    def __init__(self):
        self.active = -1
        self.worldButtons = []
        super(SelectWorld, self).__init__()
        self.worlds = savefile.getAllWorlds()
        i = 0
        for world in self.worlds:
            btn = gui.Button((self.screenWidth-150, 10+40*i, 140, 30), _('Load world'),
                             click=lambda screen, btn: screen.selectWorld(btn.key), radio='worlds', key=i)
            self.buttons.append(btn)
            self.worldButtons.append(btn)
            nameBox = gui.TextBox((10, 15+40*i), world.name)
            self.textboxes.append(nameBox)
            color = colors.LightSteelBlue if world.version == game.VERSION else colors.LightCoral
            versionBox = gui.TextBox((20+nameBox.render.get_width(), 15+40*i), 'v'+world.version, color)
            self.textboxes.append(versionBox)

            i += 1

        buttonWidth = 150
        buttonHeight = 40

        backBtn = gui.Button((self.screenWidth - buttonWidth - 10, self.screenHeight - buttonHeight - 10,
                              buttonWidth, buttonHeight), _('Back'),
                             lambda screen, btn: game.change_screen('MainMenu'), key=pygame.K_ESCAPE)
        self.buttons.append(backBtn)

    def reset(self):
        self.active = -1
        for btn in self.worldButtons:
            btn.activeOff()

    def clear(self):
        super().clear()
        game.staticSurface.fill(colors.Black)
        
    def draw(self):
        for i in range(len(self.worlds)):
            pygame.draw.rect(game.staticSurface, colors.LightBlue, (0, 5 + 40 * i, self.screenWidth, 40), 1)
            if i == self.active:
                pygame.draw.rect(game.staticSurface, colors.DarkSlateGray, (1, 6+i*40, self.screenWidth-2, 38))

        super().draw()
            
    def up(self):
        if len(self.worldButtons) == 0:
            return
        if self.active == -1 or self.active == 0:
            self.active = len(self.worldButtons)-1
        else:
            self.active -= 1
        self.activate()
            
    def down(self):
        if len(self.worldButtons) == 0:
            return
        if self.active == -1 or self.active == len(self.worldButtons)-1:
            self.active = 0
        else:
            self.active += 1
        self.activate()

    def activate(self):
        i = 0
        for btn in self.worldButtons:
            if i == self.active:
                btn.activeOn()
            else:
                btn.activeOff()
            i += 1
        self.redraw()
            
    def selectWorld(self, i):
        if len(self.worldButtons) == 0 or i == -1:
            return
        self.active = i
        game.change_screen('WorldPreview', {'world':self.worlds[self.active]})
            
    def call(self, event):
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                self.up()
            elif event.key == pygame.K_DOWN:
                self.down()
            elif event.key == pygame.K_RETURN:
                self.selectWorld(self.active)
        super().call(event)
