import pygame
import settings as Settings
import colors
import gamescreen
import images
import translate
import gui
import os
import simpleaudio as sa

WINDOW_TITLE = 'Space Roguelike'
FPS = 60
VERSION = '0.0.1'

CAMERA_SPEED = 10

settings = None
display = None
surface = None
staticSurface = None
currentScreen = None
screensCache = dict()
font = None
camera = gui.Camera((0, 0, 0))
clock = None

def init():
    '''
     Game initialisation
    '''

    global settings, display, surface, staticSurface, font, camera, clock

    settings = Settings.load()
    pygame.mixer.pre_init(44100, -16, 1, 512)
    pygame.init()
    pygame.font.init()
    pygame.mixer.init()

    pygame.display.set_caption(WINDOW_TITLE)
    pygame.display.set_icon(images.WINDOW_ICON)
    font = pygame.font.Font('res/retrocomputer.ttf', 14)
    # font = pygame.font.SysFont('Monotype', 14)
    translate.init(settings["language"])

    mode = pygame.display.list_modes()[0]
    display = pygame.display.set_mode((800,600), pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
    surface = pygame.Surface((display.get_width(), display.get_height()))
    staticSurface = pygame.Surface((display.get_width(), display.get_height()))
    camera.move((display.get_width()/2, display.get_height()/2))
    images.load()
    clock = pygame.time.Clock()

def loop():
    '''
     Main loop
    '''
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exit(0)
            elif event.type == pygame.VIDEORESIZE:
                pygame.display.set_mode(event.dict['size'], pygame.HWSURFACE | pygame.DOUBLEBUF | pygame.RESIZABLE)
                reload_screen()
            else:
                currentScreen.call(event)

        currentScreen.tick()
        flush()
        clock.tick(FPS)


def flush():
    display.blit(surface, (-camera.getBounds().left, -camera.getBounds().top))
    display.blit(staticSurface, (0, 0))
    pygame.display.update()


def reload_screen():
    screensCache.clear()
    change_screen(currentScreen.__class__.__name__, currentScreen.config, True)


def change_screen(name, config=None, forceReset=False):
    '''

    '''
    global currentScreen, surface, staticSurface

    if currentScreen:
        currentScreen.mode = gamescreen.MODE_DISABLED

    surface = pygame.Surface((display.get_width(), display.get_height()))
    surface.fill(colors.Black)
    staticSurface = pygame.Surface((display.get_width(), display.get_height()), pygame.SRCALPHA, 32).convert_alpha()

    camera.pos = (display.get_width()/2, display.get_height()/2)
    camera.zoom = 1

    if name in screensCache:
        currentScreen = screensCache[name]
        if forceReset:
            currentScreen.reset()
    else:
        currentScreen = getattr(gamescreen, name)()
        screensCache[name] = currentScreen

    currentScreen.load(config)
    currentScreen.mode = gamescreen.MODE_ACTIVE
    currentScreen.reset()
    currentScreen.clear()
    currentScreen.draw()
    flush()

