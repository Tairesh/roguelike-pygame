import game

class Unit:

    def __init__(self, t):
        self.tile = t
        self.openable = False
        self.x = self.tile.x
        self.y = self.tile.y
        self.blocked = self.tile.blocked
        self.transparent = self.tile.transparent
        self.connected = self.tile.connected
        self.interactive = True

    def update(self):
        self.tile.update()

    def onChange(self):
        self.x = self.tile.x
        self.y = self.tile.y
        self.blocked = self.tile.blocked
        self.transparent = self.tile.transparent
        self.connected = self.tile.connected

    def draw(self):
        self.tile.draw()

    def actionList(self):
        return tuple()

    def action(self, action):
        pass


class Door(Unit):
    def __init__(self, tilesByState, state='closed'):
        super().__init__(tilesByState[state])
        self.state = state
        self.openable = True
        self.tilesByState = tilesByState

    def actionList(self):
        if self.state == 'opened':
            return ('close',)
        else:
            return ('open',)

    def action(self, action):
        if action == 'open':
            self.tryOpen()
        elif action == 'close':
            self.tryClose()

    def onChange(self):
        self.tile = self.tilesByState[self.state]
        super().onChange()

    def tryOpen(self):
        self.state = 'opened'
        self.onChange()

    def tryClose(self):
        self.state = 'closed'
        self.onChange()


class ControlPanel(Unit):
    def __init__(self, t):
        super().__init__(t)

    def actionList(self):
        return ('activate', )

    def action(self, action):
        if action == 'activate':
            self.tryActivate()

    def tryActivate(self):
        game.change_screen('TestStarsystem', None, True)
