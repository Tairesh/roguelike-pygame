import milkyway

SOL_MASS = 1.98892*1E+27
STEFAN_BOLTSMAN_CONSTANT = 5.670373*1E-8


STAR = 1
ASTEROID = 2
DWARF_PLANET = 3
MOON = 4
GAS_GIANT = 5
ICE_GIANT = 6
ROCK_PLANET = 7

NAMEPARTS = {
    "en" , "la" , "can", "be" ,
    "and", "phi", "eth", "ol" ,
    "ve" , "ho" , "a"  , "lia",
    "an" , "ar" , "ur" , "mi" ,
    "in" , "ti" , "qu" , "so" ,
    "ed" , "ess", "ex" , "io" ,
    "ce" , "ze" , "fa" , "ay" ,
    "wa" , "da" , "ack", "gre"
}

SPECTRAL = {
    0: "Type'M'flare star",
    1: "Faint type'M'red star",
    2: "Type'M'red star",
    3: "Type'K'orange star",
    4: "Type'G'yellow star",
    5: "Type'F'white star",
    6: "Type'A'hot white star",
    7: "White dwarf star",
    8: "Red giant star",
    9: "Bright giant star",
    10: "Type'B'hot blue star",
    11: "Supergiant star",
    12: "Blue supergiant star",
}
SPECTRAL_COLORS = {
    (200,  0,  0),    # Type'M'flare star
    (200,  0,  0),    # Faint type'M'red star
    (200,  0,  0),    # Type'M'red star
    (200,136,  0),    # Type'K'orange star
    (200,200,  0),    # Type'G'yellow star
    (200,200,200),    # Type'F'white star
    (200,200,200),    # Type'A'hot white star
    (200,200,200),    # White dwarf star
    (200,  0,  0),    # Red giant star
    (200,200,200),    # Bright giant star
    (168,200,232),    # Type'B'hot blue star
    (192,136,  0),    # Supergiant star
    (168,200,232),    # Blue supergiant star
    (200,200,  0),    # Contact binary star
}

STARSPECTRE_CHANCE = {
    0, 0, 0, 1, 1, 1, 1, 1,
    0, 0, 0, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 3, 3, 3,
    2, 2, 2, 2, 2, 3, 3, 3,
    3, 4, 4, 4, 5, 6, 7, 8,
    3, 4, 4, 4, 5, 6, 7, 8,
    9, 9, 10, 11, 12, 0, 0,
    1, 1, 1, 1, 1, 1, 1, 1,
}

MULTISTAR_CHANCE = {
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 1, 1, 1, 1, 1,
    1, 1, 2, 2, 2, 3, 4, 5
}

