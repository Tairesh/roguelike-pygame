import json
import os
import sys

filename = os.path.join('.', 'settings.json')

default_settings = {'language': 'en'}


def rewriteFile(settings):
    with open(filename, 'w+') as file:
        file.write(json.dumps(settings))


def validate(settings):
    if not 'language' in settings:
        return False

    if not settings['language'] in ('en', 'ru'):
        return False

    return True


def correct(settings):

    if not 'language' in settings:
        print('Settings file have not key "language", using default: '+default_settings['language'])
        settings['language'] = default_settings['language']
    elif not settings['language'] in ('en', 'ru'):
        print('Unsupported language, using default: ' + default_settings['language'])
        settings['language'] = default_settings['language']

    return settings


def load():
    '''
     Returns dict with game settings
    '''
    if os.path.exists(filename):
        with open(filename) as file:
            try:
                data = json.load(file)
                if not validate(data):
                    print('Invalid settings, replacing settings.json...')
                    data = correct(data)
                    rewriteFile(data)
                return data
            except json.decoder.JSONDecodeError:
                print('Invalid settings.json, creating new file...')
                rewriteFile(default_settings)
                return default_settings
    else:
        print('Creating new settings.json...')
        rewriteFile(default_settings)
        return default_settings


def save(data):
    '''
     Save new game settings
    '''
    print('Rewriting settings.json...')
    rewriteFile(data)
