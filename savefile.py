import os
import sqlite3
import game
import utils
import json

DIR = 'save'
EXT = '.save'


def dir(f):
    return os.path.join(DIR, f)


def worldnameToFilename(name):
    return dir(name + EXT)


def listFiles():
    return [dir(f) for f in os.listdir(DIR) if os.path.isfile(dir(f)) and f.endswith(EXT)]


class Config:
    def __init__(self):
        self.name = None

    def validate(self):
        if not self.name or len(self.name) == 0:
            return False
        return True


def createNewWorld(config: Config):
    filename = worldnameToFilename(config.name)
    files = listFiles()
    if filename in files:
        print('File ' + filename + ' allready exists, aborting...')
        return False

    connection = sqlite3.connect(filename)
    cursor = connection.cursor()
    cursor.execute('PRAGMA journal_mode=WAL')
    cursor.execute('''
        CREATE TABLE system (
            name        TEXT UNIQUE NOT NULL,
            value        TEXT DEFAULT NULL
        );
            ''')

    cursor.execute('INSERT INTO system (name, value) VALUES (\'name\', \'' + config.name + '\');')
    cursor.execute('INSERT INTO system (name, value) VALUES (\'version\', \'' + game.VERSION + '\');')
    cursor.execute('INSERT INTO system (name, value) VALUES (\'time\', \'0\');')

    cursor.execute('''
        CREATE TABLE `stars` (
            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
            `name`	TEXT NOT NULL,
            `data`	TEXT NOT NULL,
            `sectorId`	INTEGER NOT NULL,
            `x`	INTEGER NOT NULL,
            `y`	INTEGER NOT NULL,
            `z`	INTEGER NOT NULL
        );
    ''')

    cursor.execute('''
        CREATE UNIQUE INDEX `coords` ON `stars` (`x`,`y`,`z`);
    ''')

    cursor.execute('''
        CREATE INDEX `sectorIdIndex` ON `stars` (`sectorId`);
    ''')

    cursor.execute('''
        CREATE TABLE `ships` (
            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
            `name`	TEXT NOT NULL,
            `data`	TEXT NOT NULL,
            `starId`	INTEGER NOT NULL,
            `r`	INTEGER NOT NULL,
            `phi`	REAL NOT NULL,
            `z`	INTEGER NOT NULL
        );    
    ''')

    cursor.execute('''
        CREATE INDEX `starIdIndex` ON `ships` (`starId`);
    ''')

    cursor.execute('''
        CREATE TABLE `players` (
            `id`	INTEGER PRIMARY KEY AUTOINCREMENT,
            `name`	TEXT NOT NULL,
            `data`	TEXT NOT NULL
        );
    ''')

    connection.commit()
    connection.close()
    return True


class Player:
    def __init__(self, id=None, name=None, data=None):
        self.id = int(id) if id else None
        self.name = name
        self.data = data

        self.scenario = 0
        self.gender = True
        self.age = 18
        self.race = 0
        self.attributes = {
            'strength': 12,
            'agility': 12,
            'intelligence': 12,
            'charisma': 12,
            'luck': 12,
        }
        self.perks = []
        self.skills = {
            'dodge': 0,
            'melee': 0,
            'throwing': 0,
            'firearms': 0,
            'handguns': 0,
            'shotguns': 0,
            'submachine guns': 0,
            'rifles': 0,
            'grenade launcher': 0,
            'turrets': 0,
            'computers': 0,
            'mechanics': 0,
            'fabrication': 0,
            'electronics': 0,
            'survival': 0,
            'first aid': 0,
            'speech': 0,
            'barter': 0,
            'swimming': 0,
            'driving': 0,
            'spaceship control': 0,
        }
        if data:
            pass

    def serialize(self):
        data = self.__dict__
        data.pop('data')
        data.pop('id')
        self.data = json.dumps(data)
        return self.name, self.data


class World:
    INT_FIELDS = {'time', }

    def __init__(self, filename):
        self.filename = filename
        cursor = self._connect()
        for name, value in cursor.execute('SELECT name, value FROM system'):
            if name in World.INT_FIELDS:
                value = int(value)
            setattr(self, name, value)
        self.connection.close()

    def _connect(self):
        self.connection = sqlite3.connect(self.filename)
        return self.connection.cursor()

    def getPlayers(self):
        cursor = self._connect()
        players = []
        for id, name, data in cursor.execute('SELECT id, name, data from players'):
            player = Player(id, name, data)
            players.append(player)
        self.connection.close()
        return players

    def appendPlayer(self, player):
        name, data = player.serialize()
        data.replace('\'', '"')
        cursor = self._connect()
        cursor.execute('INSERT INTO `players`(`id`,`name`,`data`) VALUES (NULL, \''+name+'\', \''+data+'\');')
        self.connection.commit()
        self.connection.close()

    @property
    def displayedTime(self):
        seconds = self.time//30
        minutes = seconds//60
        hours = minutes//60
        days = minutes//24
        hours -= days*24
        minutes -= days*24*60+hours*60
        seconds -= days*24*60*60+hours*60*60+minutes*60
        return 'Day '+str(days+1)+". "+utils.intToZeroStarted(hours)+":"+utils.intToZeroStarted(minutes)+":"+utils.intToZeroStarted(seconds)

def getAllWorlds():
    files = listFiles()
    worlds = []
    for filename in files:
        world = World(filename)
        worlds.append(world)

    return worlds
