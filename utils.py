import game


def intToZeroStarted(n):
    return str(n) if n > 9 else "0"+str(n)


def displayPointToSurface(point):
    return (point[0] + game.camera.getBounds().left,
            point[1] + game.camera.getBounds().top)
